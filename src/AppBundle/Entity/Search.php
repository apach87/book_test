<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;


class Search
{

    private $fromPrice;

    private $toPrice;

    private $category;

    /**
     * @return mixed
     */
    public function getFromPrice()
    {
        return $this->fromPrice;
    }

    /**
     * @param mixed $fromPrice
     */
    public function setFromPrice($fromPrice)
    {
        $this->fromPrice = $fromPrice;
    }

    /**
     * @return mixed
     */
    public function getToPrice()
    {
        return $this->toPrice;
    }

    /**
     * @param mixed $toPrice
     */
    public function setToPrice($toPrice)
    {
        $this->toPrice = $toPrice;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }


}

