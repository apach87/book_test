<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BookHasCategory
 *
 * @ORM\Table(name="book_has_category")
 * @ORM\Entity
 */
class BookHasCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Book
     *
     * @ORM\ManyToOne(targetEntity="Book", inversedBy="categories")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     * @ORM\OrderBy({"title" = "ASC"})
     */
    private $book;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="books", cascade= {"all"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $category;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer")
     */
    private $priority;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Book
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param Book $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category = null)
    {
        $this->category = $category;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return BookHasCategory
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}

