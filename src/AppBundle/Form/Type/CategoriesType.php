<?php
/**
 * Created by PhpStorm.
 * User: pnowak
 * Date: 05.04.16
 * Time: 12:33
 */
// src/AppBundle/Form/Type/GenderType.php
namespace AppBundle\Form\Type;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\Form\ChoiceList\ORMQueryBuilderLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Entity\Category;

class CategoriesType extends AbstractType
{
    /**
     * @var CategoryRepository $categoryRepository
     */
    protected $categoryRepository;


    protected $list = array();
    /**
     * CategoriesType constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'class' => Category::class,
            'choice_label' => function(Category $category) {
                /** @var Category $category */
                return $category->getNameWithLevel();
            },
            'required'  => false,
        ));
    }


    public function getParent()
    {
        return EntityType::class;
    }

}