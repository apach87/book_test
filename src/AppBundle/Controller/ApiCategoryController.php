<?php
namespace AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \AppBundle\Entity\Advertisement;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Category;

/**
 * Admin controller.
 *
 * @Route("/api")
 */
class ApiCategoryController extends Controller
{


    /**
     * Finds and displays a Advertisement entity.
     *
     * @Route("/category", name="api_categories_tree", options = { "expose" = true })
     * @Method("GET")
     */
    public function CategoriesAction()
    {
        $convertService = $this->get('convert_format_category_service');
        $result = $convertService->covertToJStree();
        return new JsonResponse($result);
    }

    /**
     * Finds and displays a Advertisement entity.
     *
     * @Route("/category", name="api_category", options = { "expose" = true })
     * @Method({"POST","PUT"})
     */
    public function CategoryAction(Request $request)
    {
        $convertService = $this->get('convert_format_category_service');
   
        $entity = $convertService->convertToCategory($request->get('node'), $request->isMethod('PUT'));
        $em = $this->getDoctrine()->getManager();
        if($request->isMethod('PUT')){
            $em->persist($entity);
        }
        $em->flush();
        
        return new JsonResponse(
            array('result' => $entity->getId())
        );
    }

    /**
     * Finds and displays a Advertisement entity.
     *
     * @Route("/category", name="api_delete_category", options = { "expose" = true })
     * @Method("DELETE")
     */
    public function DeleteCategoryAction(Request $request)
    {
        $category = $request->get('node');
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('AppBundle:Category');

        $entity = $repo->findOneBy(array('id'=>$category['id']));
        $em->remove($entity);
        $em->flush();
        return new JsonResponse('success');
    }

}