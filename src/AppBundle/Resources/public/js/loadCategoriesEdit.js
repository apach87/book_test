var t = $('#tree');
var urlData = {
    "url": "{{ path('api_categories_tree') }}",
    "data": function (node) {
        return {"id": node.id};
    }
};
t.jstree({
    'core': {
        "check_callback": true, // enable all modifications
        'data': urlData

    },
    "themes": {
        "variant": "large",
        "dots": false // no connecting dots between dots
    },
    "plugins": ["state", "dnd", "contextmenu", "unique", "sort"],


})
    .on('loaded.jstree', function () {
        t.jstree('open_all');
    })
    .bind("select_node.jstree", function (e, data) {
        return data.instance.toggle_node(data.node);
    }).bind("move_node.jstree", function (e, data) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: Routing.generate('api_category'),
        data: {node: data.node}
    });
}).bind("create_node.jstree", function (e, dNode, pos) {
    $.ajax({
        type: 'PUT',
        dataType: "json",
        url: Routing.generate('api_category'),
        data: {node: dNode.node},
        success: function (datas) {
            t.jstree(true).set_id(dNode.node,datas.result);

        }
    });

}).bind("rename_node.jstree", function (e, data) {
    $.ajax({
        type: 'POST',
        dataType: "json",
        url: Routing.generate('api_category'),
        data: {node: data.node}

    });
}).bind("delete_node.jstree", function (e, data) {
    $.ajax({
        type: 'DELETE',
        dataType: "json",
        url: Routing.generate('api_category'),
        data: {node: data.node}
    });
});
$('#new_category').click(function () {
    t.jstree(true).create_node("#");
});