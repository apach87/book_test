var catList = $("#categories-list");
var t = $('#tree');
catList.sortable({
        change: function (event, ui) {
            var index = ui.placeholder.index();
            ui.item.data('priority', index);
        }
    }
);
catList.disableSelection();

var urlData = {
    "url": Routing.generate('api_categories_tree'),
    "data": function (node) {
        return {"id": node.id};
    }
};
t.jstree({
    'core': {
        "check_callback": true, // enable all modifications
        'data': urlData
    },
    "themes": {
        "variant": "large",
        "dots": false // no connecting dots between dots
    },
    "plugins": ["state", "unique", "sort"]

}).on('loaded.jstree', function () {
    t.jstree('open_all');
})
.bind("select_node.jstree", function (e, data) {
    addCategoryToList(data);
});

var addCategoryToList = function (data){
    var count = $('.cat-element').length || 0;
    var NewContent = '<li class="ui-state-default cat-element" data-id="' + data.node.id + '" ' +
        'data-priority="' + (count) + '">' +
        '<input type="hidden" id="appbundle_book_categories_' + count + '_priority" name="appbundle_book[categories][' + count + '][priority]" value="' + count + '">' +
        '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>' +
        '<input type="hidden" id="appbundle_book_categories_' + count + '_category" value="' + data.node.id + '" name="appbundle_book[categories][' + count + '][category]" required="required" readonly="readonly">' +
        data.node.text + '</li>';
    var selectedElement = $('.cat-element[data-id="' + data.node.id + '"]');
    if(selectedElement.length === 0){
        catList.append(NewContent);
    } else {
        selectedElement.remove();
    }
};