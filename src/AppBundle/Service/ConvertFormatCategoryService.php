<?php
namespace AppBundle\Service;

use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;

/**
 * Class ConvertFormatCategoryService
 * this service makes conversions Category <-> jstree array
 * @package AppBundle\Service
 */
class ConvertFormatCategoryService
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * ConvertFormatCategoryService constructor.
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * convert Category list to jstree friendly array
     * @return array
     */
    public function covertToJStree() : array
    {
        $result = [];
        $entities = $this->categoryRepository->findByTree();
        foreach($entities as $category){
            $result[] = $this->addCategory($category);
        }

        return $result;
    }

    private function addCategory(Category $entity)
    {
        $result = array();
        $result['id'] = $entity->getId();
        $result['text'] = $entity->getName();
        $children = $entity->getChildren();

        if(!$children->isEmpty()){
            $result['children'] = array();
            foreach ($children as $c){
                $result['children'][] = $this->addCategory($c);
            }
        }
        return $result;
    }

    /**
     * convert jstree object to category in order to create or modify category
     * @param $category
     * @param bool $isNew
     * @return Category|null|object
     */
    public function convertToCategory($category, $isNew = true) : ?Category
    {

        if($category['text'] != null){
            if($isNew){
                $entity = new Category();
            } else {
                $entity = $this->categoryRepository->findOneBy(array('id'=>$category['id']));
            }

            $parent = $category['parent'];
            if($parent != '#'){
                $parent = $this->categoryRepository->findOneBy(array('id'=>$parent));
                $entity->setParent($parent);
            }
            $entity->setName($category['text']);

            return $entity;
        }
        return null;
    }

    
}