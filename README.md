book test
============

Przed uruchomieniem należy utworzyć bazę danych mysql.

W celu uruchomienia projektu należy uruchomić:
```sh
composer install
php bin/console assets:install --symlink
php bin/console doctrine:schema:update --force
```

W projekcie jest nietypowa obsługa kategorii:

- w menu "Category" jest drzewo oraz przycisk pozwalający na dodawanie i edycje kategorii (prawym przyciskiem myszy)
- w edycji oraz dodawaniu "Book" jest również drzewo kategorii którego po wybraniu dodaje do listy po prawej stronie, drugi raz wybranie usuwa je
- w liście kategorii w dodawniu oraz edycji "Book" jest możliwe ustawnie kolejności jako prioritet poprzez Drag and Drop

Todo:
- usuwanie kategorii z "Book"
- zmienie wartości prioritetu podczas zmiany kolejności w liście
- więcej testów
- szlify wyglądu