<?php
namespace Testes\AppBundle\Service;

/**
 * Created by PhpStorm.
 * User: pnowak
 * Date: 24.03.16
 * Time: 10:21
 */
use AppBundle\Entity\Category;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Service\ConvertFormatCategoryService;
use Doctrine\Common\Collections\ArrayCollection;
use \Mockery as m;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;


class ConvertFormatCategoryServiceTest extends KernelTestCase
{
    public function tearDown()
    {
        m::close();
    }

    /**
     * unit test
     */
    public function testConvertToJsTreeOneElement()
    {
        $result = array(
            0 => array(
                'id' => 1,
                'text' => 'cat'
            )
        );
        $arrayCollection = m::mock(ArrayCollection::class,
            array(
                'isEmpty' => true
            ));
        $categoryMock = m::mock(Category::class,
            array(
                'getId' => 1,
                'getName' => 'cat',
                'getChildren' => $arrayCollection
            ));
        $categoryArray = [$categoryMock];
        $categoryRepositoryMock = m::mock(CategoryRepository::class,
            array(
                'findByTree' => $categoryArray
            ));
        $service = new ConvertFormatCategoryService($categoryRepositoryMock);
        $this->assertSame($service->covertToJStree(), $result);
    }

    /**
     * unit test
     */
    public function testConvertToJsTreeEmpty()
    {
        $result = array();

        $categoryArray = [];
        $categoryRepositoryMock = m::mock(CategoryRepository::class,
            array(
                'findByTree' => $categoryArray
            ));
        $service = new ConvertFormatCategoryService($categoryRepositoryMock);
        $this->assertSame($service->covertToJStree(), $result);
    }

//    /**
//     * unit test
//     */
//    public function testConvertToElement()
//    {
//        $array = array(
//                'text' => 1,
//                'parent' => '#'
//        );
//        $arrayCollection = m::mock(ArrayCollection::class,
//            array(
//                'isEmpty' => true
//            ));
//        $categoryMock = m::mock(Category::class,
//            array(
//                'setName' => null,
//                'setParent' => null
//            ));
//
//        $categoryArray = [];
//        $categoryRepositoryMock = m::mock(CategoryRepository::class,
//            array(
//                'findByTree' => $categoryArray
//            ));
//        $service = new ConvertFormatCategoryService($categoryRepositoryMock);
//        $this->assertSame($service->convertToCategory($array, true), $categoryMock);
//    }

}